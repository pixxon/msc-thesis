Mérések futtatása:  
- ```docker login registry.gitlab.com```  
- ```docker pull registry.gitlab.com/pixxon/msc-thesis/run-env:latest```  
- ```docker run -it registry.gitlab.com/pixxon/msc-thesis/run-env:latest```  
- ```./bin/gcc/queue-measure```  
- ```./bin/gcc/suffix-array-measure```  
- ```./bin/gcc/burrows-wheeler-measure```  
