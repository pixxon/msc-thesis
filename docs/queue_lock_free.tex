\section{Zárolásmentes}

\subsection{Elmélet}
\paragraph{}
A következő implementációnkban el akarjuk kerülni a zárak használatát és ezzel együtt a kritikus szakaszokat. Az adatszerkezetünket úgy kell megalkotnunk, hogy minden változást lokálisan hajtsunk végre és az eredményt egyetlen utasítással tegyük véglegessé. Ez a megközelítés sokszor nem jelent gyorsulást a program végrehajtásában, az elvetett lokális számolások miatt még lassabb is lehet, azonban biztosan elkerüljük a holtpont kialakulását. Kritikus rendszereknél egy biztonsági feltétel fontosabb, mint a sebességvesztés.
\paragraph{}
Használjuk a modern processzorok fejlett utasításkészletét. A nagyobb méretű adatok egy utasításban történő mozgatásával, el tudjuk érni, hogy az adatok olvasása és írása atomikusan történjen, vagyis nem tudunk inkonzisztens állapotban lévő változót olvasni. Ilyen lehet például egy memóriaterületre mutató változó, melyet ha inkonzisztens állapotban olvasunk, a programunk hibás állapotba kerül és terminál. Másik fontos utasítás a Compare-and-Swap, mely három paramétert vár, egy írandó területet, egy feltételezett jelenlegi értéket és az újat, eredménye, hogy sikeres-e a művelet. Az írás csak akkor történik meg, ha a jelenlegi érték megegyezik az elvárttal, ellenkező esetben nem történik módosítás. Ennek segítségével tudjuk a lokális módosításinkat egyetlen utasítással globálissá tenni, annak a feltétele mellett, hogy más nem tett olyan módosítást, mellyel elrontanánk az adatstruktúra invariánsát.
\paragraph{}
Ilyen adatszerkezet megtervezése nem egyszerű, a zárolás elkerülése nélkül is tudunk olyan állapotot elérni, amikor egyik szál akadályozza egy másik haladását. Így, az eddigi megvalósításokkal ellenben, egy kutatás \cite{michael_scott} eredményét dolgozom fel.
\paragraph{}
A sorba beszúrás sokkal komplikáltabbá vált, hiába atomikusak az olvasások, többször is ellenőriznünk kell, hogy még mindig az a valóság, amit mi korábban láttunk. Először a szükséges mutatókat olvassuk ki, ha változás történt közben, akkor újra kell olvasunk a változók értékét. Sikeres olvasás után ellenőrizzük, hogy a lista végén lévő elemet látjuk-e. Amennyiben ez teljesül, a lista végére próbáljuk szúrni a mi elemünket, mely ha sikerül, befejezzük a próbálkozásokat és megpróbáljuk léptetni az osztály mutatóját. Ezt nem biztos, hogy mi fogjuk megtenni, ugyanis abban az esetben, ha egy másik szál nem a lista végére mutató elemet látja, akkor megpróbálja ő maga léptetni. Erre azért van szükség, hogy a szálak ne tudják akaratlanul blokkolni egymást. Ha az új elem beszúrása és az osztály mutatójának módosítása közben függesztjük fel egy szál végrehajtását, akkor más szálról nem tudnánk új elemet beszúrni.
\paragraph{}
Egy elem kivétele is hasonlóan működik, a szükséges változókat olvassuk ki először, hogy lokálisan tudjuk őket kezelni. Itt is megtörténhet, hogy az utolsó elem mutatója rossz helyen áll, ekkor mi is léptetjük. Erre nem lenne szükség, hiszen mondhatnánk, hogy üres a sor, de így jobb eredményt érhetünk el. Ha van elem a sorunkban, akkor megpróbáljuk azt kifűzni a sorból és ha sikerült, akkor visszatérünk az elemmel.

\subsection{Implementáció}
\paragraph{}
Először az osztály belső szerkezetét kellett módosítani, lásd~\ref{fig:queue_lock_free_members} ahhoz, hogy az elemeket atomikus műveletekkel érjük el. Az \lstinline{std::atomic} osztály biztosítja ezt, amennyiben az adatunk triviálisan másolható. Ez a zárolásmentes adatszerkezetünknek nem elegendő, ugyanis az osztály használhat zárolást, hogy biztosítsa a helyes működést. Ellenőriznünk kell, hogy az osztályunk elkerüli a zárak használatát, ezt egy osztályszintű változó, \lstinline{std::atomic<T>::is_always_lock_free} segítségével tehetjük meg.
\paragraph{}
Az elmélet leképezése viszonylag egyszerűen hajtható végre, a Compare-and-Swap utasítást támogatja az \lstinline{std::atomic} osztály. Az első paramétere az általunk várt érték, a második, hogy mire szeretnénk megváltoztatni. Az osztály szintén támogatja a változók írását és olvasását a \lstinline{std::atomic<T>::load} és \lstinline{std::atomic<T>::store} metódusokkal.
\paragraph{}
A fenti elméletet használva valósítottam meg az elem beszúrás és kivétel műveleteket, az implementáció a~\ref{fig:queue_lock_free_push} és~\ref{fig:queue_lock_free_pop} ábrákon láthatóak. A végtelen ciklus miatt nem biztos, hogy a szálak véges lépésben végre fogják hajtani a műveletet, de az említett kutatásban igazolták, hogy bármely ütemezés mellett legalább egy szál véges lépésben végez.
\paragraph{}
Az feldolgozott publikációban a szemétgyűjtő létezését feltételezik, így a memóriafelszabadítással nem foglalkozik. Mivel a C++ nyelv nem támogatja ezt, nekünk kell felszabadítani a lefoglalt memóriát. Ez a probléma nem triviális, létezik zárolásmentes megoldás \cite{hazard_ptr}, azonban a kutatásom ezzel nem foglalkozik. A jelenlegi C++ szabvány is támogatja már a referencia számolt mutatókat és ezeket atomikus elérését, azonban ez a következő szabványban lesz teljes és akár zárolás mentesen használható \cite{conc_ts}.

\begin{figure}[H]
	\centering
	\begin{lstlisting}
template <typename DATA_TYPE>
struct LOCK_FREE_QUEUE : I_QUEUE<DATA_TYPE>
{
	void Push(DATA_TYPE aData) override;
	DATA_TYPE Pop() override;
	private:
	struct NODE
	{
		DATA_TYPE mData;
		std::atomic<NODE*> mNext;
		
		NODE(DATA_TYPE aData, NODE* aNext) :
		mData(std::move(aData)),
		mNext(std::move(aNext))
		{}
	};
	
	std::atomic<NODE*> mFront{new NODE({}, nullptr)};
	std::atomic<NODE*> mRear{mFront.load()};
};
	\end{lstlisting}
	\caption{}
	\label{fig:queue_lock_free_members}
	\small
	A zárolásmentes adatszerkezet az atomikus adattagokkal.
\end{figure}
\begin{figure}[H]
	\centering
	\begin{lstlisting}
void Push(DATA_TYPE aData) override
{
	auto NewNode = new NODE(std::move(aData), nullptr);
	auto Tail = mRear.load();
	while(true)
	{
		Tail = mRear.load();
		auto Next = Tail->mNext.load();
		if (Tail == mRear.load())
		{
			if (Next == nullptr)
			{
				if (Tail->mNext.compare_exchange_weak(Next, NewNode))
				{
					break;
				}
			}
			else
			{
				mRear.compare_exchange_weak(Tail, Next);
			}
		}
	}
	mRear.compare_exchange_weak(Tail, NewNode);
}
	\end{lstlisting}
	\caption{}
	\label{fig:queue_lock_free_push}
	\small
	Beszúrás, mely végtelenszer újrapróbálkozhat, amíg nem sikerül.
\end{figure}
\begin{figure}[H]
	\centering
	\begin{lstlisting}
DATA_TYPE Pop() override
{
	while(true)
	{
		auto Head = mFront.load();
		auto Tail = mRear.load();
		auto Next = Head->mNext.load();
		if (Head == mFront.load())
		{
			if (Head == Tail)
			{
				if (Next == nullptr)
				{
					return {};
				}
				mRear.compare_exchange_weak(Tail, Next);
			}
			else
			{
				auto Data = Next->mData;
				if (mFront.compare_exchange_weak(Head, Next))
				{
					return Data;
				}
			}
		}
	}
}
	\end{lstlisting}
	\caption{}
	\label{fig:queue_lock_free_pop}
	\small
	Elem kivétele a sorból, szintén végtelen próbálkozási lehetőséggel.
\end{figure}
