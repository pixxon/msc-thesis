\section{Burrows-Wheeler transzformáció}

\subsection{Definíció}
\paragraph{}
A Burrows-Wheeler transzformáció egy bijektív leképezés, mely egy $N$ hosszú sztringhez egy $N+1$ hosszú sztringet rendel. A különböző karakterek száma megegyezik, csak a sorrendjük változik meg és egy speciális $\$$ karaktert illesztünk be.
\paragraph{}
Egy szóhoz képezzük az összes ciklikus eltoltját, vagyis $A = \{S[i,N+1] \circ \$ \circ S[0, i+1] | i \in [0, \dots, N]\}$, ahol $N$ a szó hossza, a $\circ$ pedig a konkatenálás művelet. Ezután a kapott sztringeket lexikálisan növekvő sorrendbe rendezzük, majd az utolsó karakterekből képezzük a transzformáltat, $\forall i \in [1, \dots, N-] : S'[i] = A[i][N]$.
\paragraph{}
A fenti algoritmus nem hatékony, a szavak előállítása tárhelyigényes, a rendezés pedig időigényes. Helyette először a szuffix tömböt állítjuk elő, jelöljük $A$-val. Ezután lineáris időben tudjuk képezni a transzformáltat, $\forall i \in [0, \dots, (N-1)] : S'[i] = S[A[i] - 1]$, ha $S[i] > 1$, különben $\$$. Az előbb feltételeztük, hogy magát az indexet tartalmazza a szuffix tömb, amennyiben magát a részszót, úgy $A[i]$ helyett $(N - len(A[i]))$-t kell használnunk, ahol a $len$ függvény megadja a szó hosszát.
\paragraph{}
A transzformációt főként tömörítésnél alkalmazzuk, nem mint tömörítő algoritmus, hanem mint egy előkészítő lépés. Az előállt permutációban az azonos karakterek egymás mellé kerülnek, melyet a tömörítő algoritmusok hatékonyan redukálnak. A bzip2 \cite{bzip2} program a Huffman kódolás előtt végzi el az átalakítást, ezzel lassabban, de hatékonyan tudja tömöríteni az információt.

\subsection{Szekvenciális implementáció}
\paragraph{}
Felhasználjuk a szuffix tömb előállító megvalósításunk az előző szekcióból, szintén az indexeket használó szekvenciális változatot használva. Így a teljes implementáció egyszerű, csak végig kell mennünk az elemeken és kiszámoljuk a megfelelő karaktert. Az egyetlen probléma ezzel a megoldással, hogy a bemeneti elemeink szinte véletlenszerű permutációját kaptunk, így sérül a lokalitás elve, mellyel performanciát vesztünk.
\paragraph{}
A leképezéshez szintén használhatunk beépített algoritmust, az \lstinline{std::transform} függvényt, mely iterátorokkal dolgozik, így jobb eredményeket érhetünk el vele, mint az indexelt változattal.
\begin{figure}[H]
	\centering
	\begin{lstlisting}
std::string BurrowsWheeler(const std::string& aInput)
{
	auto Suffixes = SuffixArray(aInput);

	const auto Convert = [&](const auto& aIndex)
	{
		return aIndex > 0 ? aInput.at(aIndex - 1) : '$' ;
	};

	auto Result = aInput;
	for(auto Index = 0u; Index < aInput.size(); ++Index)
	{
		Result.at(Index) = Convert(Suffixes.at(Index));
	}

	return Result;
}
	\end{lstlisting}
	\caption{}
	\label{fig:bwt_my_seq}
	\small
	Transzformáció az indexelt szuffix tömb segítségével.
\end{figure}

\subsection{Modern megközelítés}
\paragraph{}
Szintén fel tudjuk használni az \lstinline{std::string_view} adatszerkezetre épülő implementációt, egyedül egy leképezést kell készítenünk az indexelésre, ez a $I = len(suffix)$, ahol $N$ az sztringünk hossza. Ez várhatóan nem jár jelentős performancia veszteséggel, mert hatékonyan, konstans időben ki tudjuk számolni egy szó hosszát.
\paragraph{}
Előzőhöz hasonlóan, most is készítünk egy STL-n épülő változatot, hogy össze tudjuk vetni a saját implementációnkkal.
\begin{figure}[H]
	\centering
	\begin{lstlisting}
std::string BurrowsWheeler(const std::string& aInput)
{
	auto Suffixes = SuffixArray(aInput);
	
	const auto Convert = [&](const std::string_view& aSuffix)
	{
		return aSuffix.size() > 0 ? aInput.at(aSuffix.size() - 1) : '$';
	};
	
	auto Result = aInput;
	for(auto Index = 0u; Index < aInput.size(); ++Index)
	{
		Result.at(Index) = Convert(Suffixes.at(Index));
	}
	
	return Result;
}
	\end{lstlisting}
	\caption{}
	\label{fig:bwt_my_modern}
	\small
	\lstinline{std::string_view} felhasználásával történő konverzió.
\end{figure}
\begin{figure}[H]
	\centering
	\begin{lstlisting}
std::string BurrowsWheeler(const std::string& aInput)
{
	auto Suffixes = SuffixArray(aInput);
	
	const auto Convert = [&](const std::string_view& aSuffix)
	{
		return aSuffix.size() > 0 ? aInput.at(aSuffix.size() - 1) : '$';
	};
	
	auto Result = aInput;
	std::transform(Suffixes.begin(), Suffixes.end(), Result.begin(), Convert);
	
	return Result;
}
	\end{lstlisting}
	\caption{}
	\label{fig:bwt_std_modern}
	\small
	Az \lstinline{std::transform} használata.
\end{figure}

\subsection{Párhuzamos megvalósítás}
\paragraph{}
A feladatot értelmezhetjük úgy, hogy $N$ darab elemen kell elvégeznünk ugyanazt a műveletet. Ekkor tehetnénk, hogy $N$ darab szálnak kiosztjuk az $N$ darab elemet, majd a végén begyűjtjük, ez azonban nem optimális, mert $N$ jelentősen nagyobb, mint a magok száma, így a szálak egymás elől vennék el a erőforrást. Másik probléma ezzel, hogy egy szál elindításának költsége magasabb, mint egy feladat elvégzése, így rosszabbul teljesítenénk, mint a szekvenciális megoldás.
\paragraph{}
Az ötlet az, hogy az $N$ hosszú sztringet felosztjuk $M$ darabra, ahol $M$ a magok száma. Ezután indítunk $(M-1)$ darab szálat és kiosztunk mindegyiknek egy $N/M$ méretű intervallumot. A fennmaradó részt azon a szálon fogjuk kiszámolni, amelyről indítottuk a többit. Megvárjuk, míg az összes szál befejezi a számolást, ezután az elkészült eredménnyel visszatérünk. Az ábrán látható, ahogy egy 11 hosszú vektort 4 szálon párhuzamosan dolgozunk fel.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{pics/taskfarm}
	\caption{}
	\small
	Task-farm általános felépítése.
\end{figure}
\paragraph{}
Lehetőségünk van a beépített \lstinline{std::transform} algoritmust is párhuzamosan végrehajtani. Létezik egy túlterhelt változata, mely első paramétere a végrehajtási mód. Három lehetséges értéke van: \lstinline{std::execution::seq}, \lstinline{std::execution::par} és \lstinline{std::execution::par_unseq}. Az első az alapértelmezett, ilyenkor szekvenciálisan hajtódik végre az utasítás.
\paragraph{}
A párhuzamos végrehajtás esetén megengedjük, hogy az egyes elemeket külön szálon dolgozza fel, például ha két vektor elemeit szeretnénk feldolgozni, akkor az megtörténhet a \ref{fig:ex_execute_par} ábrán látható módon.
\paragraph{}
\begin{minipage}{.5\textwidth}
	\centering
	\begin{figure}[H]
		\centering
		\begin{minipage}{.4\textwidth}
			\centering
			\begin{algorithmic}
				\State \textbf{'A' szál:}
				\State \textbf{load} $x[i]$
				\State \textbf{load} $y[i]$
				\State \textbf{mul}
				\State \textbf{store} $z[i]$
			\end{algorithmic}
		\end{minipage}%
		\begin{minipage}{.4\textwidth}
			\centering
			\begin{algorithmic}
				\State \textbf{'B' szál:}
				\State \textbf{load} $x[j]$
				\State \textbf{load} $y[j]$
				\State \textbf{mul}
				\State \textbf{store} $z[j]$
			\end{algorithmic}
		\end{minipage}
		\caption{}
		\label{fig:ex_execute_par}
		\small
		Párhuzamos végrehajtás.
	\end{figure}
\end{minipage}%
\begin{minipage}{.5\textwidth}
	\begin{figure}[H]
		\centering
		\begin{minipage}{.6\textwidth}
			\centering
			\begin{algorithmic}
				\State \textbf{'A' szál:}
				\State \textbf{load} $x[i..(i+3)]$
				\State \textbf{load} $y[i..(i+3)]$
				\State \textbf{mul} // egyszerre
				\State \textbf{store} $z[i..(i+3)]$
			\end{algorithmic}
		\end{minipage}
		\caption{}
		\label{fig:ex_execute_par_unseq}
		\small
		Vektorizált végrehajtás.
	\end{figure}
\end{minipage}
\paragraph{}
Vektorizált párhuzamos végrehajtás esetén lehetőséget adunk arra, hogy az utasítások akár egy szálon belül is átfedjék egymást. Ezzel egyszerre több adatot is be tudunk tölteni a memóriából a regiszterekbe, majd elvégezni rajtuk a szükséges műveleteket, akár egy utasítással. Egyre több olyan utasítást támogatnak a modern processzorok, melyekkel egyszerre több adaton tudjuk ugyanazt a műveletet elvégezni, ilyenkor ez a megközelítés jelentős javuláshoz vezet, a lokalitás és a megspórolt utasítások miatt.

\begin{figure}[H]
	\centering
	\begin{lstlisting}
std::string BurrowsWheeler(const std::string& aInput)
{
	auto Suffixes = SuffixArray(aInput);
	auto Result = aInput;

	const auto Convert = [&](const std::string_view& aSuffix)
	{
		return aSuffix.size() > 0 ? aInput.at(aSuffix.size() - 1) : '$';
	};

	if (aInput.size() > 500)
	{
		std::atomic<uint32_t> Counter{0};

		const auto Worker = [&](auto Start, auto End)
		{
			for(auto Index = Start; Index < End; ++Index)
			{
				Result.at(Index) = Convert(Suffixes.at(Index));
			}
		};

		std::vector<std::thread> Threads(std::thread::hardware_concurrency() - 1);

		auto Start = 0u;
		const auto Part = aInput.size() / (Threads.size() + 1);
		for(auto& Thread : Threads)
		{
			Thread = std::thread(Worker, Start, Start + Part);
			Start = Start + Part;
		}
		Worker(Start, aInput.size());

		for(auto& Thread : Threads)
		{
			Thread.join();
		}
	}
	else
	{
		for(auto Index = 0u; Index < aInput.size(); ++Index)
		{
			Result.at(Index) = Convert(Suffixes.at(Index));
		}
	}

	return Result;
}
	\end{lstlisting}
	\caption{}
	\label{fig:bwt_my_par}
	\small
	Task-farm párhuzamos implementálása.
\end{figure}
\begin{figure}[H]
	\centering
	\begin{lstlisting}
std::string BurrowsWheeler(const std::string& aInput)
{
	auto Suffixes = SuffixArray(aInput);

	const auto Convert = [&](const std::string_view& aSuffix)
	{
		return aSuffix.size() > 0 ? aInput.at(aSuffix.size() - 1) : '$';
	};

	auto Result = aInput;
	std::transform(std::execution::par, Suffixes.begin(), Suffixes.end(), 
	                                    Result.begin(), Convert);

	return Result;
}
	\end{lstlisting}
	\caption{}
	\label{fig:bwt_std_par}
	\small
	A transzformáció párhuzamos végrehajtása beépített algoritmus segítségével.
\end{figure}
