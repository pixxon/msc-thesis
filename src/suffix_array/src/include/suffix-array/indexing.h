#pragma once

#include <vector>
#include <string>
#include <cstdint>
#include <algorithm>

namespace suffix_array
{
	struct INDEXING
	{
		template <typename Iterator>
		void MergeSort(const std::string& aInput, Iterator aBegin, Iterator aEnd)
		{
			const auto Size = std::distance(aBegin, aEnd);
			
			if(1 < Size)
			{
				const auto Middle = aBegin + Size / 2;
				
				MergeSort(aInput, aBegin, Middle);
				MergeSort(aInput, Middle, aEnd);
				
				const auto Comp = [&](const auto& aFirst, const auto& aSecond)
				{
					for(auto Index = 0u; Index < (aInput.size() - std::max(aFirst, aSecond)); ++Index)
					{
						if (aInput.at(Index + aFirst) < aInput.at(Index + aSecond))
						{
							return true;
						}
						else if (aInput.at(Index + aFirst) > aInput.at(Index + aSecond))
						{
							return false;
						}
					}
					return aFirst > aSecond;
				};
				
				std::inplace_merge(aBegin, Middle, aEnd, Comp);
			}
		}
		
		std::vector<uint32_t> GenerateSuffixes(const std::string& aInput)
		{
			auto Result = std::vector<uint32_t>();
			Result.reserve(aInput.size());
			
			for(auto Index = 0u; Index < aInput.size(); ++Index)
			{
				Result.emplace_back(Index); 
			}
			
			return Result;
		}
		
		std::vector<uint32_t> SuffixArray(const std::string& aInput)
		{
			auto Suffixes = GenerateSuffixes(aInput);
			MergeSort(aInput, Suffixes.begin(), Suffixes.end());
			return Suffixes;
		}
	};
}
