#pragma once

#include <vector>
#include <string_view>
#include <cstdint>
#include <algorithm>

namespace suffix_array
{
	struct SEQUENTIAL
	{
		template <typename Iterator>
		void MergeSort(Iterator aBegin, Iterator aEnd)
		{
			const auto Size = std::distance(aBegin, aEnd);
			
			if(1 < Size)
			{
				const auto Middle = aBegin + Size / 2;
				
				MergeSort(aBegin, Middle);
				MergeSort(Middle, aEnd);
				
				std::inplace_merge(aBegin, Middle, aEnd);
			}
		}
		
		std::vector<std::string_view> GenerateSuffixes(std::string_view aInput)
		{
			auto Result = std::vector<std::string_view>();
			Result.reserve(aInput.size());
			
			for(auto Index = 0u; Index < aInput.size(); ++Index)
			{
				Result.emplace_back(aInput.data() + Index, aInput.size() - Index); 
			}
			
			return Result;
		}
		
		std::vector<std::string_view> SuffixArray(std::string_view aInput)
		{
			auto Suffixes = GenerateSuffixes(aInput);
			MergeSort(Suffixes.begin(), Suffixes.end());
			return Suffixes;
		}
	};
}
