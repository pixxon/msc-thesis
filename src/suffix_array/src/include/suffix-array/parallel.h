#pragma once

#include <vector>
#include <string_view>
#include <cstdint>
#include <thread>

namespace suffix_array
{
	constexpr unsigned int Log2(unsigned int aNumber)
	{
		return aNumber == 1 ? 0 : Log2(aNumber - 1) + 1;
	}
	
	struct PARALLEL
	{
		template <typename Iterator>
		void Sort(Iterator aBegin, Iterator aEnd, uint32_t aDepth = 1)
		{
			const auto Size = std::distance(aBegin, aEnd);
			
			if(200 < Size && aDepth < Log2(4))
			{
				const auto Middle = aBegin + Size / 2;
				
				auto t = std::thread([&](){ return Sort(Middle, aEnd, aDepth + 1); });
				Sort(aBegin, Middle, aDepth + 1);
				t.join();
				
				std::inplace_merge(aBegin, Middle, aEnd);
			}
			else if(1 < Size)
			{
				const auto Middle = aBegin + Size / 2;
				
				Sort(aBegin, Middle, aDepth + 1);
				Sort(Middle, aEnd, aDepth + 1);
				
				std::inplace_merge(aBegin, Middle, aEnd);
			}
		}
		
		std::vector<std::string_view> GenerateSuffixes(std::string_view aInput)
		{
			auto Result = std::vector<std::string_view>();
			Result.reserve(aInput.size());
			
			for(auto Index = 0u; Index < aInput.size(); ++Index)
			{
				Result.emplace_back(aInput.data() + Index, aInput.size() - Index); 
			}
			
			return Result;
		}
		
		std::vector<std::string_view> SuffixArray(std::string_view aInput)
		{
			auto Suffixes = GenerateSuffixes(aInput);
			Sort(Suffixes.begin(), Suffixes.end());
			return Suffixes;
		}
	};
}