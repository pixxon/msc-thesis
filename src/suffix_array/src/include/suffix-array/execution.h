#pragma once

#include <vector>
#include <string>
#include <string_view>

#include <pstl/execution>
#include <pstl/algorithm>

namespace suffix_array
{
	struct EXECUTION
	{
		std::vector<std::string_view> GenerateSuffixes(std::string_view aInput)
		{
			auto Result = std::vector<std::string_view>();
			Result.reserve(aInput.size());
			
			for(auto Index = 0u; Index < aInput.size(); ++Index)
			{
				Result.emplace_back(aInput.data() + Index, aInput.size() - Index); 
			}
			
			return Result;
		}
		
		std::vector<std::string_view> SuffixArray(std::string_view aInput)
		{
			auto Result = GenerateSuffixes(aInput);
			std::sort(std::execution::par, Result.begin(), Result.end());
			return Result;
		}
	};
}
