#pragma once

#include <vector>
#include <string>
#include <string_view>
#include <algorithm>

namespace suffix_array
{
	struct STANDARD
	{
		std::vector<std::string_view> GenerateSuffixes(std::string_view aInput)
		{
			auto Result = std::vector<std::string_view>();
			Result.reserve(aInput.size());
			
			for(auto Index = 0u; Index < aInput.size(); ++Index)
			{
				Result.emplace_back(aInput.data() + Index, aInput.size() - Index); 
			}
			
			return Result;
		}
		
		std::vector<std::string_view> SuffixArray(std::string_view aInput)
		{
			auto Result = GenerateSuffixes(aInput);
			std::sort(Result.begin(), Result.end());
			return Result;
		}
	};
}
