#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <suffix-array/standard.h>
#include <suffix-array/sequential.h>
#include <suffix-array/parallel.h>
#include <suffix-array/execution.h>

template <typename CREATOR_TYPE>
struct SuffixArrayTest : ::testing::Test
{
	CREATOR_TYPE mCreator;
};

using CreatorTypes =
::testing::Types<
	suffix_array::STANDARD,
	suffix_array::SEQUENTIAL,
	suffix_array::PARALLEL,
	suffix_array::EXECUTION
>;
TYPED_TEST_CASE(SuffixArrayTest, CreatorTypes);

TYPED_TEST(SuffixArrayTest, Banana)
{
	auto& Creator = this->mCreator;
	
	const auto Result = Creator.SuffixArray("banana$");
	
	EXPECT_THAT(Result, ::testing::ElementsAreArray({"$", "a$", "ana$", "anana$", "banana$", "na$", "nana$"}));
}

int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
