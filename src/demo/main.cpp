#include <vector>
#include <atomic>

#include <pstl/execution>
#include <pstl/algorithm>

#include <benchmark/benchmark.h>

auto ExecPar = [](benchmark::State& aState)
{
	for (auto _ : aState)
	{
		std::vector<std::atomic<int>> k(10000);
		std::for_each(std::execution::par, k.begin(), k.end(), [](auto& e){ e.fetch_add(1, std::memory_order_relaxed); });
	}
};

auto ExecSeq = [](benchmark::State& aState)
{
	for (auto _ : aState)
	{
		std::vector<std::atomic<int>> k(10000);
		std::for_each(std::execution::seq, k.begin(), k.end(), [](auto& e){ e.fetch_add(1, std::memory_order_relaxed); });
	}
};

auto ExecUnSeq = [](benchmark::State& aState)
{
	for (auto _ : aState)
	{
		std::vector<std::atomic<int>> k(10000);
		std::for_each(std::execution::par_unseq, k.begin(), k.end(), [](auto& e){ e.fetch_add(1, std::memory_order_relaxed); });
	}
};

int main(int argc, char** argv)
{
	benchmark::RegisterBenchmark("ExecPar", ExecPar)->Iterations(100000)->UseRealTime();
	benchmark::RegisterBenchmark("ExecSeq", ExecSeq)->Iterations(100000)->UseRealTime();
	benchmark::RegisterBenchmark("ExecUnSeq", ExecUnSeq)->Iterations(100000)->UseRealTime();
	
	benchmark::Initialize(&argc, argv);
	benchmark::RunSpecifiedBenchmarks();
}