#include <thread>
#include <vector>

#include <gtest/gtest.h>

#include <queue/i_queue.hpp>
#include <queue/std_queue.hpp>
#include <queue/raw_queue.hpp>
#include <queue/seq_queue.hpp>
#include <queue/blocking_queue.hpp>
#include <queue/fine_queue.hpp>
#include <queue/lock_free_queue.hpp>
#include <queue/wait_free_queue.hpp>

template <typename QUEUE_TYPE>
struct QueueTest : ::testing::Test
{
	QUEUE_TYPE mQueue;
};

template <>
struct QueueTest<WAIT_FREE_QUEUE<int>> : ::testing::Test
{
	WAIT_FREE_QUEUE<int> mQueue{4};
};

using QueueTypes =
::testing::Types<
	SEQ_QUEUE<int>,
	RAW_QUEUE<int>,
	STD_QUEUE<int>,
	BLOCKING_QUEUE<int>,
	FINE_QUEUE<int>,
	LOCK_FREE_QUEUE<int>,
	WAIT_FREE_QUEUE<int>
>;
TYPED_TEST_CASE(QueueTest, QueueTypes);

TYPED_TEST(QueueTest, Sequential)
{
	auto& Queue = this->mQueue;
	
	for(auto I = 0u; I < 100; ++I)
	{
		Queue.Push(I);
	}
	
	for(auto I = 0u; I < 100; ++I)
	{
		auto Elem = Queue.Pop();
		EXPECT_EQ(Elem, I);
	}
}

TYPED_TEST(QueueTest, SequentialTwice)
{
	auto& Queue = this->mQueue;
	
	for(auto I = 0u; I < 100; ++I)
	{
		Queue.Push(I);
	}
	
	for(auto I = 0u; I < 100; ++I)
	{
		const auto Elem = Queue.Pop();
		EXPECT_EQ(Elem, I);
	}
	
	for(auto I = 0u; I < 100; ++I)
	{
		Queue.Push(I);
	}
	
	for(auto I = 0u; I < 100; ++I)
	{
		const auto Elem = Queue.Pop();
		EXPECT_EQ(Elem, I);
	}
}

TYPED_TEST(QueueTest, DISABLED_Empty)
{
	auto& Queue = this->mQueue;
	
	for(auto I = 0u; I < 100; ++I)
	{
		const auto Elem = Queue.Pop();
		EXPECT_EQ(Elem, 0);
	}
}

TYPED_TEST(QueueTest, ParallelProducer)
{
	if(!decltype(this->mQueue)::IS_THREAD_SAFE)
	{
		return;
	}
	
	auto& Queue = this->mQueue;
	
	const auto Producer = [&](uint32_t Count)
	{
		for(auto I = 0u; I < Count; ++I)
		{
			Queue.Push(I);
		}
	};
	
	std::vector<std::thread> Threads(4);
	for(auto& Thread : Threads)
	{
		Thread = std::thread(Producer, 100);
	}
	for(auto& Thread : Threads)
	{
		Thread.join();
	}
	
	std::array<uint8_t, 100> Counter{{0}};
	for(auto I = 0u; I < 4 * 100; ++I)
	{
		const auto Elem = Queue.Pop();
		Counter[Elem]++;
	}
	
	for(auto Count : Counter)
	{
		EXPECT_EQ(Count, 4);
	}
}

TYPED_TEST(QueueTest, ParallelConsumer)
{
	if(!decltype(this->mQueue)::IS_THREAD_SAFE)
	{
		return;
	}
	
	auto& Queue = this->mQueue;
	
	for(auto I = 0u; I < 100; ++I)
	{
		Queue.Push(I);
		Queue.Push(I);
		Queue.Push(I);
		Queue.Push(I);
	}
	
	std::array<std::atomic<uint8_t>, 100> Counter{{0}};
	const auto Consumer = [&](uint32_t Count)
	{
		for(auto I = 0u; I < Count; ++I)
		{
			const auto Elem = Queue.Pop();
			Counter[Elem]++;
		}
	};
	
	std::vector<std::thread> Threads(4);
	for(auto& Thread : Threads)
	{
		Thread = std::thread(Consumer, 100);
	}
	for(auto& Thread : Threads)
	{
		Thread.join();
	}
	
	for(auto& Count : Counter)
	{
		EXPECT_EQ(Count.load(), 4);
	}
}

int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}