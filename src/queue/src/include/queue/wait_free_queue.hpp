#pragma once

#include <atomic>

#include "i_queue.hpp"

template <typename DATA_TYPE>
struct WAIT_FREE_QUEUE : I_QUEUE<DATA_TYPE>
{
	static constexpr bool IS_THREAD_SAFE = true;
	
	WAIT_FREE_QUEUE(const unsigned int aThreadCount) :
		mStates(aThreadCount),
		mThreads(aThreadCount)
	{
		static_assert(std::atomic<NODE*>::is_always_lock_free);
		static_assert(std::atomic<OPERATION*>::is_always_lock_free);
		
		for(auto& State : mStates)
		{
			State.store(new OPERATION{-1, false, true, nullptr});
		}
		for (auto& Thread : mThreads)
		{
			Thread.store(nullptr);
		}
	}
	
	auto GetThreadID()
	{
		int Result = 0;
		for (unsigned int i = 0; i < mThreads.size(); i++)
		{
			auto& Thread = mThreads.at(i);
			auto* ThreadID = Thread.load();
			
			if(ThreadID != nullptr && *ThreadID == std::this_thread::get_id())
			{
				Result = i;
				break;
			}
			else if(ThreadID == nullptr)
			{
				if(Thread.compare_exchange_weak(ThreadID, new std::thread::id(std::this_thread::get_id())))
				{
					Result = i;
					break;
				}
			}
		}
		return Result;
	}
	
	void Push(DATA_TYPE aData)
	{
		const auto ThreadID = GetThreadID();
		const auto Phase = GetMaxPhase() + 1;
		
		auto* NewNode = new NODE(std::move(aData), nullptr, ThreadID);
		auto* NewOperation = new OPERATION(Phase, true, true, NewNode);
		mStates.at(ThreadID).store(NewOperation);
		
		Helper(Phase);
		FinishPush();
	}
	
	DATA_TYPE Pop()
	{
		const auto ThreadID = GetThreadID();
		const auto Phase = GetMaxPhase() + 1;
		
		auto* NewOperation = new OPERATION(Phase, true, false, nullptr);
		mStates.at(ThreadID).store(NewOperation);
		
		Helper(Phase);
		FinishPop();
		
		const auto* Node = mStates.at(ThreadID).load()->mNode;
		if (Node == nullptr)
		{
			return{};
		}
		return Node->mNext.load()->mData;
	}
	
	~WAIT_FREE_QUEUE()
	{
		while(mFront.load() != nullptr)
		{
			auto Temp = mFront.load();
			mFront.store(Temp->mNext.load());
			delete Temp;
		}
	}
	
private:
	struct NODE
	{
		DATA_TYPE mData;
		std::atomic<NODE*> mNext;
		
		uint32_t mEnqTid;
		std::atomic<uint32_t> mDeqTid;
		
		NODE(DATA_TYPE aData, NODE* aNext, uint32_t aEnqTid) :
			mData(std::move(aData)),
			mNext{aNext},
			mEnqTid(aEnqTid),
			mDeqTid{static_cast<uint32_t>(-1)}
		{}
	};
	
	struct OPERATION
	{ 
		int32_t mPhase; 
		bool mPending; 
		bool mEnqueue; 
		NODE* mNode;
		
		OPERATION(int32_t aPhase, bool aPending, bool aEnqueue, NODE* aNode) :
			mPhase(aPhase),
			mPending(aPending),
			mEnqueue(aEnqueue),
			mNode(std::move(aNode))
		{} 
	};
	
	void Helper(int32_t aPhase)
	{
		for (unsigned int i = 0; i < mStates.size(); i++)
		{
			auto* Operation = mStates.at(i).load(); 
			if (Operation->mPending && Operation->mPhase <= aPhase)
			{
				if (Operation->mEnqueue)
				{
					HelpPush(i, aPhase);
				}
				else
				{
					HelpPop(i, aPhase);
				}
			}
		}
	}
	
	auto GetMaxPhase()
	{
		const auto PhaseCompare = [](const auto& First, const auto& Second)
		{
			return First.load()->mPhase < Second.load()->mPhase;
		};
		
		const auto MaxElement = std::max_element(mStates.begin(), mStates.end(), PhaseCompare);
		return MaxElement->load()->mPhase;
	}
	
	bool IsStillPending(int aTid, long aPhase)
	{
		const auto* Operation = mStates.at(aTid).load();
		return Operation->mPending && Operation->mPhase <= aPhase;
	}
	
	void HelpPush(int aTid, long aPhase)
	{
		while (IsStillPending(aTid, aPhase))
		{
			auto* Last = mRear.load();
			auto* Next = Last->mNext.load();
			if (Last == mRear.load())
			{
				if (Next == nullptr)
				{
					if (IsStillPending(aTid, aPhase))
					{
						if (Last->mNext.compare_exchange_weak(Next, mStates.at(aTid).load()->mNode))
						{
							FinishPush();
							return;
						}
					}
				}
				else
				{
					FinishPush();
				}
			}
		}
	}
	
	void FinishPush()
	{
		auto* Last = mRear.load();
		auto* Next = Last->mNext.load();
		if (Next != nullptr)
		{
			auto Tid = Next->mEnqTid;
			auto* CurrentDesc = mStates.at(Tid).load();
			if (Last == mRear.load() && CurrentDesc->mNode == Next)
			{
				auto* NewDesc = new OPERATION(CurrentDesc->mPhase, false, true, Next);
				if(!mStates.at(Tid).compare_exchange_weak(CurrentDesc, NewDesc))
				{
					delete NewDesc;
				}
				mRear.compare_exchange_weak(Last, Next);
			}
		}
	}
	
	void HelpPop(int aTid, long aPhase)
	{
		while (IsStillPending(aTid, aPhase))
		{
			auto* First = mFront.load();
			auto* Next = First->mNext.load();
			auto* Last = mRear.load();
			if (First == mFront.load())
			{
				if (First == Last)
				{
					if (Next == nullptr)
					{
						auto* CurrentDesc = mStates.at(aTid).load();
						if (Last == mRear.load() && IsStillPending(aTid, aPhase))
						{
							auto* NewDesc = new OPERATION(CurrentDesc->mPhase, false, false, nullptr);
							mStates.at(aTid).compare_exchange_weak(CurrentDesc, NewDesc);
						}
					}
					else
					{
						FinishPush();
					}
				}
				else
				{
					auto* CurrentDesc = mStates.at(aTid).load();
					auto* Node = CurrentDesc->mNode;
					if (!IsStillPending(aTid, aPhase))
						break;
					if (First == mFront.load() && Node != First)
					{
						auto* NewDesc = new OPERATION(CurrentDesc->mPhase, true, false, First);
						if (!mStates.at(aTid).compare_exchange_weak(CurrentDesc, NewDesc))
						{
							delete NewDesc;
							continue;
						}
					}
					uint32_t ThreadID = std::numeric_limits<uint32_t>::max();
					First->mDeqTid.compare_exchange_weak(ThreadID, aTid);
					FinishPop();
				}
			}
		}
	}
	
	void FinishPop()
	{
		auto* First = mFront.load();
		auto* Next = First->mNext.load();
		auto Tid = First->mDeqTid.load();
		if (Tid != std::numeric_limits<uint32_t>::max())
		{
			auto* CurrentDesc = mStates.at(Tid).load();
			if (First == mFront.load() && Next != nullptr)
			{
				auto* NewDesc = new OPERATION(CurrentDesc->mPhase, false, false, CurrentDesc->mNode);
				if(!mStates.at(Tid).compare_exchange_weak(CurrentDesc, NewDesc))
				{
					delete NewDesc;
				}
				mFront.compare_exchange_weak(First, Next);
			}
		}
	}
	
	std::atomic<NODE*> mFront{new NODE({}, nullptr, std::numeric_limits<uint32_t>::max())};
	std::atomic<NODE*> mRear{mFront.load()};
	std::vector<std::atomic<OPERATION*>> mStates;
	std::vector<std::atomic<std::thread::id*>> mThreads;
};
