#pragma once

#include <queue>
#include <list>

#include "i_queue.hpp"

template <typename DATA_TYPE>
struct STD_QUEUE : I_QUEUE<DATA_TYPE>
{
	static constexpr bool IS_THREAD_SAFE = false;
	
	DATA_TYPE Pop() override
	{
		const auto Elem = mQueue.front();
		mQueue.pop();
		return Elem;
	}
	
	void Push(DATA_TYPE aData) override
	{
		mQueue.push(aData);
	}
	
private:
	std::queue<DATA_TYPE, std::list<DATA_TYPE>> mQueue;
};
