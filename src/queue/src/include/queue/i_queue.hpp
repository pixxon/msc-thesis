#pragma once

template <typename DATA_TYPE>
struct I_QUEUE
{
	virtual DATA_TYPE Pop() = 0;
	virtual void Push(DATA_TYPE) = 0;
	
	virtual ~I_QUEUE() = default;
};
