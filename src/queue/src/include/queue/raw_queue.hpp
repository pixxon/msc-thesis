#pragma once

#include <memory>

#include "i_queue.hpp"

template <typename DATA_TYPE>
struct RAW_QUEUE : I_QUEUE<DATA_TYPE>
{
	static constexpr bool IS_THREAD_SAFE = false;
	
	void Push(DATA_TYPE aData) override
	{
		auto* Temp = new NODE(std::move(aData), nullptr);
		
		if(mRear == nullptr)
		{
			mFront = std::move(Temp);
			mRear = mFront;
		}
		else
		{
			mRear->mNext = std::move(Temp);
			mRear = mRear->mNext;
		}
	}
	
	DATA_TYPE Pop() override
	{
		if(mFront == nullptr)
		{
			return {};
		}
		
		const auto Data = mFront->mData;
		
		if(mFront == mRear)
		{
			mFront = nullptr;
			mRear = nullptr;
		}
		else
		{
			auto* Temp = mFront;
			mFront = mFront->mNext;
			Temp->mNext = nullptr;
			delete Temp;
		}
		
		return Data;
	}
	
	~RAW_QUEUE()
	{
		while(mFront != nullptr)
		{
			auto* Temp = mFront;
			mFront = mFront->mNext;
			Temp->mNext = nullptr;
			delete Temp;
		}
	}
	
private:
	struct NODE
	{
		DATA_TYPE mData;
		NODE* mNext;
		
		NODE(DATA_TYPE aData, NODE* aNext) :
			mData(std::move(aData)),
			mNext(std::move(aNext))
		{}
		
		~NODE()
		{
			if (mNext != nullptr)
			{
				delete mNext;
			}
		}
	};
	
	NODE* mFront = nullptr;
	NODE* mRear = nullptr;
};
