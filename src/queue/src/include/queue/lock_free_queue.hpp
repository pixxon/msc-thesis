#pragma once

#include <atomic>

#include "i_queue.hpp"

template <typename DATA_TYPE>
struct LOCK_FREE_QUEUE : I_QUEUE<DATA_TYPE>
{
	static constexpr bool IS_THREAD_SAFE = true;
	
	LOCK_FREE_QUEUE()
	{
		static_assert(std::atomic<NODE*>::is_always_lock_free);
	}
	
	void Push(DATA_TYPE aData) override
	{
		auto NewNode = new NODE(std::move(aData), nullptr);
		auto Tail = mRear.load();
		while(true)
		{
			Tail = mRear.load();
			auto Next = Tail->mNext.load();
			if (Tail == mRear.load())
			{
				if (Next == nullptr)
				{
					if (Tail->mNext.compare_exchange_weak(Next, NewNode))
					{
						break;
					}
				}
				else
				{
					mRear.compare_exchange_weak(Tail, Next);
				}
			}
		}
		mRear.compare_exchange_weak(Tail, NewNode);
	}
	
	DATA_TYPE Pop() override
	{
		while(true)
		{
			auto Head = mFront.load();
			auto Tail = mRear.load();
			auto Next = Head->mNext.load();
			if (Head == mFront.load())
			{
				if (Head == Tail)
				{
					if (Next == nullptr)
					{
						return {};
					}
					mRear.compare_exchange_weak(Tail, Next);
				}
				else
				{
					auto Data = Next->mData;
					if (mFront.compare_exchange_weak(Head, Next))
					{
//						delete Head;
						return Data;
						break;
					}
				}
			}
		}
	}
	
	~LOCK_FREE_QUEUE()
	{
		while(mFront.load() != nullptr)
		{
			auto Temp = mFront.load();
			mFront.store(Temp->mNext.load());
			delete Temp;
		}
	}
	
private:
	struct NODE
	{
		DATA_TYPE mData;
		std::atomic<NODE*> mNext;
		
		NODE(DATA_TYPE aData, NODE* aNext) :
			mData(std::move(aData)),
			mNext(std::move(aNext))
		{}
	};
	
	std::atomic<NODE*> mFront{new NODE({}, nullptr)};
	std::atomic<NODE*> mRear{mFront.load()};
};
