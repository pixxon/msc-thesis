#pragma once

#include <memory>
#include <mutex>

#include "i_queue.hpp"

template <typename DATA_TYPE>
struct BLOCKING_QUEUE : I_QUEUE<DATA_TYPE>
{
	static constexpr bool IS_THREAD_SAFE = true;
	
	void Push(DATA_TYPE aData) override
	{
		std::unique_lock<std::mutex> Lock(mMutex);
		
		auto Temp = std::make_unique<NODE>(std::move(aData), nullptr);
		
		if(mRear == nullptr)
		{
			mFront = std::move(Temp);
			mRear = mFront.get();
		}
		else
		{
			mRear->mNext = std::move(Temp);
			mRear = mRear->mNext.get();
		}
	}
	
	DATA_TYPE Pop() override
	{
		std::unique_lock<std::mutex> Lock(mMutex);
		
		if(mFront == nullptr)
		{
			return {};
		}
		
		const auto Data = mFront->mData;
		
		if(mFront.get() == mRear)
		{
			mFront = nullptr;
			mRear = nullptr;
		}
		else
		{
			mFront = std::move(mFront->mNext);
		}
		
		return Data;
	}

	~BLOCKING_QUEUE()
	{
		while(mFront != nullptr)
		{
			mFront = std::move(mFront->mNext);
		}
	}
	
private:
	struct NODE
	{
		DATA_TYPE mData;
		std::unique_ptr<NODE> mNext;
		
		NODE(DATA_TYPE aData, std::unique_ptr<NODE> aNext) :
			mData(std::move(aData)),
			mNext(std::move(aNext))
		{}
	};
	
	std::unique_ptr<NODE> mFront = nullptr;
	NODE* mRear = nullptr;
	std::mutex mMutex;
};
