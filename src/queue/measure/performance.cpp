#include <cmath>
#include <thread>

#include <benchmark/benchmark.h>

#include <queue/i_queue.hpp>
#include <queue/std_queue.hpp>
#include <queue/raw_queue.hpp>
#include <queue/seq_queue.hpp>
#include <queue/blocking_queue.hpp>
#include <queue/fine_queue.hpp>
#include <queue/lock_free_queue.hpp>
#include <queue/wait_free_queue.hpp>

auto Producer = [](benchmark::State& aState, auto& aQueue)
{
	const auto IsPrime = [](uint32_t aNumber)
	{
		bool IsPrime = true;
		for(auto Div = 2u; Div < std::sqrt(aNumber) && IsPrime; ++Div)
		{
			IsPrime = ((aNumber % Div) != 0);
		}
		return IsPrime;
	};
	
	const auto NextPrime = [&](uint32_t aNumber)
	{
		while(!IsPrime(aNumber))
		{
			++aNumber;
		}
		return aNumber;
	};
	
	uint32_t Prime = 2;
	for (auto _ : aState)
	{
		aQueue.Push(Prime);
		Prime = NextPrime(Prime);
	}
};

auto Consumer = [](benchmark::State& aState, auto& aQueue)
{
	for (auto _ : aState)
	{
		aQueue.Pop();
	}
};

auto Multi = [](benchmark::State& aState, auto& aQueue)
{
	const auto IsPrime = [](uint32_t aNumber)
	{
		bool IsPrime = true;
		for(auto Div = 2u; Div < std::sqrt(aNumber) && IsPrime; ++Div)
		{
			IsPrime = ((aNumber % Div) != 0);
		}
		return IsPrime;
	};
	
	const auto NextPrime = [&](uint32_t aNumber)
	{
		while(!IsPrime(aNumber))
		{
			++aNumber;
		}
		return aNumber;
	};
	
	uint32_t Prime = 2;
	for (auto _ : aState)
	{
		aQueue.Push(Prime);
		Prime = NextPrime(Prime);
		aQueue.Push(Prime);
		Prime = NextPrime(Prime);
		aQueue.Pop();
		aQueue.Pop();
	}
};

template <template<typename> typename QUEUE_TYPE>
void RegisterBenchmark(std::string QueueName)
{
	static QUEUE_TYPE<int> Queue;
	const auto ProducerWrapper = [&](benchmark::State& aState)
	{
		return Producer(aState, Queue);
	};
	
	const auto ConsumerWrapper = [&](benchmark::State& aState)
	{
		return Consumer(aState, Queue);
	};
	
	const auto MultiWrapper = [&](benchmark::State& aState)
	{
		return Multi(aState, Queue);
	};
	
	benchmark::RegisterBenchmark((QueueName + "/Producer").c_str(), ProducerWrapper)->Threads(1)->Iterations(100000)->UseRealTime();
	benchmark::RegisterBenchmark((QueueName + "/Consumer").c_str(), ConsumerWrapper)->Threads(1)->Iterations(100000)->UseRealTime();
	benchmark::RegisterBenchmark((QueueName + "/Multi").c_str(), MultiWrapper)->Threads(1)->Iterations(100000)->UseRealTime();
	
	for(auto I = 2u; QUEUE_TYPE<int>::IS_THREAD_SAFE && I <= std::thread::hardware_concurrency(); I *= 2)
	{
		benchmark::RegisterBenchmark((QueueName + "/Producer").c_str(), ProducerWrapper)->Threads(I)->Iterations(100000)->UseRealTime();
		benchmark::RegisterBenchmark((QueueName + "/Consumer").c_str(), ConsumerWrapper)->Threads(I)->Iterations(100000)->UseRealTime();
		benchmark::RegisterBenchmark((QueueName + "/Multi").c_str(), MultiWrapper)->Threads(I)->Iterations(100000)->UseRealTime();
	}
}


template <>
void RegisterBenchmark<WAIT_FREE_QUEUE>(std::string QueueName)
{
	static WAIT_FREE_QUEUE<int> Queue{std::thread::hardware_concurrency()};
	const auto ProducerWrapper = [&](benchmark::State& aState)
	{
		return Producer(aState, Queue);
	};
	
	const auto ConsumerWrapper = [&](benchmark::State& aState)
	{
		return Consumer(aState, Queue);
	};
	
	const auto MultiWrapper = [&](benchmark::State& aState)
	{
		return Multi(aState, Queue);
	};
	
	benchmark::RegisterBenchmark((QueueName + "/Producer").c_str(), ProducerWrapper)->Threads(1)->Iterations(100000)->UseRealTime();
	benchmark::RegisterBenchmark((QueueName + "/Consumer").c_str(), ConsumerWrapper)->Threads(1)->Iterations(100000)->UseRealTime();
	benchmark::RegisterBenchmark((QueueName + "/Multi").c_str(), MultiWrapper)->Threads(1)->Iterations(100000)->UseRealTime();
	
	for(auto I = 2u; WAIT_FREE_QUEUE<int>::IS_THREAD_SAFE && I <= std::thread::hardware_concurrency(); I *= 2)
	{
		benchmark::RegisterBenchmark((QueueName + "/Producer").c_str(), ProducerWrapper)->Threads(I)->Iterations(100000)->UseRealTime();
		benchmark::RegisterBenchmark((QueueName + "/Consumer").c_str(), ConsumerWrapper)->Threads(I)->Iterations(100000)->UseRealTime();
		benchmark::RegisterBenchmark((QueueName + "/Multi").c_str(), MultiWrapper)->Threads(I)->Iterations(100000)->UseRealTime();
	}
}

int main(int argc, char** argv)
{
	RegisterBenchmark<RAW_QUEUE>("RAW_QUEUE");
	RegisterBenchmark<SEQ_QUEUE>("SEQ_QUEUE");
	RegisterBenchmark<STD_QUEUE>("STD_QUEUE");
	RegisterBenchmark<BLOCKING_QUEUE>("BLOCKING_QUEUE");
	RegisterBenchmark<FINE_QUEUE>("FINE_QUEUE");
	RegisterBenchmark<LOCK_FREE_QUEUE>("LOCK_FREE_QUEUE");
	RegisterBenchmark<WAIT_FREE_QUEUE>("WAIT_FREE_QUEUE");
	
	benchmark::Initialize(&argc, argv);
	benchmark::RunSpecifiedBenchmarks();
}
