#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <burrows-wheeler/sequential.h>
#include <burrows-wheeler/parallel.h>
#include <burrows-wheeler/execution.h>
#include <burrows-wheeler/standard.h>
#include <burrows-wheeler/indexing.h>

template <typename CREATOR_TYPE>
struct SuffixArrayTest : ::testing::Test
{
	CREATOR_TYPE mCreator;
};

using CreatorTypes =
::testing::Types<
	SEQUENTIAL,
	PARALLEL,
	EXECUTION,
	STANDARD,
	INDEXING
>;
TYPED_TEST_CASE(SuffixArrayTest, CreatorTypes);

TYPED_TEST(SuffixArrayTest, Banana)
{
	auto& Creator = this->mCreator;
	
	const auto Result = Creator.BurrowsWheeler("abaaba$");
	
	EXPECT_THAT(Result, "abba$aa");
}

int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
