#pragma once

#include <vector>
#include <string>
#include <string_view>

#include <pstl/execution>
#include <pstl/algorithm>

#include <suffix-array/execution.h>

struct EXECUTION
{
	std::string BurrowsWheeler(const std::string& aInput)
	{
		auto Suffixes = suffix_array::EXECUTION().SuffixArray(aInput);
		
		const auto Convert = [&](const std::string_view& aSuffix)
		{
			return aSuffix.size() > 0 ? aInput.at(aSuffix.size() - 1) : aInput.at(aInput.size());
		};
		
		auto Result = aInput;
		std::transform(std::execution::par, Suffixes.begin(), Suffixes.end(), Result.begin(), Convert);
		
		return Result;
	}
};
