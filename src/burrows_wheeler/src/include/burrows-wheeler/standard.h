#pragma once

#include <vector>
#include <string>
#include <string_view>
#include <algorithm>

#include <suffix-array/standard.h>

struct STANDARD
{
	std::string BurrowsWheeler(const std::string& aInput)
	{
		auto Suffixes = suffix_array::STANDARD().SuffixArray(aInput);
		
		const auto Convert = [&](const std::string_view& aSuffix)
		{
			return aSuffix.size() > 0 ? aInput.at(aSuffix.size() - 1) : '$';
		};
		
		auto Result = aInput;
		std::transform(Suffixes.begin(), Suffixes.end(), Result.begin(), Convert);
		
		return Result;
	}
};
