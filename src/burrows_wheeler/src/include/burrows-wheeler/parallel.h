#pragma once

#include <vector>
#include <string>
#include <string_view>
#include <cstdint>
#include <atomic>
#include <thread>
#include <iostream>

#include <suffix-array/parallel.h>

struct PARALLEL
{
	std::string BurrowsWheeler(const std::string& aInput)
	{
		auto Suffixes = suffix_array::PARALLEL().SuffixArray(aInput);
		auto Result = aInput;
		
		const auto Convert = [&](const std::string_view& aSuffix)
		{
			return aSuffix.size() > 0 ? aInput.at(aSuffix.size() - 1) : aInput.at(aInput.size());
		};
		
		if (aInput.size() > 500)
		{
			std::atomic<uint32_t> Counter{0};
			
			const auto Worker = [&](auto Start, auto End)
			{
				for(auto Index = Start; Index < End; ++Index)
				{
					Result.at(Index) = Convert(Suffixes.at(Index));
				}
			};
			
			std::vector<std::thread> Threads(std::thread::hardware_concurrency() - 1);
			
			auto Start = 0u;
			const auto Part = aInput.size() / (Threads.size() + 1);
			for(auto& Thread : Threads)
			{
				Thread = std::thread(Worker, Start, Start + Part);
				Start = Start + Part;
			}
			Worker(Start, aInput.size());
			
			for(auto& Thread : Threads)
			{
				Thread.join();
			}
		}
		else
		{
			for(auto Index = 0u; Index < aInput.size(); ++Index)
			{
				Result.at(Index) = Convert(Suffixes.at(Index));
			}
		}
		
		return Result;
	}
};
