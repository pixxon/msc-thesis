#pragma once

#include <vector>
#include <string>
#include <string_view>
#include <map>
#include <cstdint>
#include <numeric>
#include <future>

#include <suffix-array/indexing.h>

struct INDEXING
{
	std::string BurrowsWheeler(const std::string& aInput)
	{
		auto Suffixes = suffix_array::INDEXING().SuffixArray(aInput);
		
		const auto Convert = [&](const auto& aIndex)
		{
			return aIndex > 0 ? aInput.at(aIndex - 1) : '$' ;
		};
		
		auto Result = aInput;
		for(auto Index = 0u; Index < aInput.size(); ++Index)
		{
			Result.at(Index) = Convert(Suffixes.at(Index));
		}
		
		return Result;
	}
};
