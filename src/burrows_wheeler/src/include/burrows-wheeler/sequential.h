#pragma once

#include <vector>
#include <string>
#include <string_view>
#include <map>
#include <cstdint>
#include <numeric>
#include <future>

#include <suffix-array/sequential.h>

struct SEQUENTIAL
{
	std::string BurrowsWheeler(const std::string& aInput)
	{
		auto Suffixes = suffix_array::SEQUENTIAL().SuffixArray(aInput);
		
		const auto Convert = [&](const std::string_view& aSuffix)
		{
			return aSuffix.size() > 0 ? aInput.at(aSuffix.size() - 1) : aInput.at(aInput.size());
		};
		
		auto Result = aInput;
		for(auto Index = 0u; Index < aInput.size(); ++Index)
		{
			Result.at(Index) = Convert(Suffixes.at(Index));
		}
		
		return Result;
	}
};
