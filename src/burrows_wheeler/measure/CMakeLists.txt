PROJECT(burrows-wheeler-measure)

SET(SOURCES performance.cpp)

ADD_EXECUTABLE(${PROJECT_NAME} ${SOURCES})
TARGET_LINK_LIBRARIES(${PROJECT_NAME} burrows-wheeler rt ${GBENCH_LIBRARY})
